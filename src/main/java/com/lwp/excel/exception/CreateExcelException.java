package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/13
 * @time: 9:22
 * @description:
 */
public class CreateExcelException extends RuntimeException {


    private final Integer code = -10003;

    public CreateExcelException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
