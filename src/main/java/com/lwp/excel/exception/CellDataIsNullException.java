package com.lwp.excel.exception;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/21
 * @time: 10:50
 * @description:
 */
public class CellDataIsNullException extends RuntimeException {

    private final Integer code = -10004;

    public CellDataIsNullException(String message) {
        super(message);
    }

    public Integer getCode() {
        return code;
    }
}
