package com.lwp.excel.style;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/22
 * @time: 11:17
 * @description:
 */
public interface ValignStyles {
    short VERTICAL_TOP = 0; //上端对齐
    short VERTICAL_CENTER = 1;//垂直居中
    short VERTICAL_BOTTOM = 2;//下端对齐
    short VERTICAL_JUSTIFY = 3;//垂直的证明
}
