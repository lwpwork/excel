package com.lwp.excel.resolver.impl;

import com.lwp.excel.resolver.ExcelAble;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/9
 * @time: 16:40
 * @description: 需要生成Excel的实体类，必须继承BaseExcelVO或者 实现ExcelAble
 */
public class BaseExcelVO implements ExcelAble {
}
