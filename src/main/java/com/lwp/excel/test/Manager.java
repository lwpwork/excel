package com.lwp.excel.test;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Title;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/15
 * @time: 9:35
 * @description:
 */
@Data
public class Manager implements ExcelAble {

    @Cell(value = "店长姓名",groups = Clerk.GroupA.class)
    private String name;

    @Cell(value = "性别",groups = Clerk.GroupA.class)
    private String sex;

    @Cell(value = "工作经验",groups = Clerk.GroupA.class)
    private String jobExperience;

    public Manager(String name, String sex, String jobExperience) {
        this.name = name;
        this.sex = sex;
        this.jobExperience = jobExperience;
    }

    public Manager() {
    }
}
