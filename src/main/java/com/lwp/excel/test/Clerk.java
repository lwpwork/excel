package com.lwp.excel.test;

import com.lwp.excel.annotation.*;
import com.lwp.excel.annotation.Font;
import com.lwp.excel.resolver.ExcelAble;
import com.lwp.excel.style.ExcelColors;
import lombok.Data;

import java.awt.*;

/**
 * Copyright (C) @2019 GuangDong Eshore Technology Co. Ltd
 *
 * @author: Administrator
 * @version: 1.0
 * @date: 2019/8/13
 * @time: 10:54
 * @description:
 */
@Data
//@Sheet(name = "{}的员工报表",isField = true,value = {"name"})
//@Title("员工统计报表")
//@Font(fontHeightInPoints = 13,fontColor = ExcelColors.GREEN)
//@Style(backgroundColor = ExcelColors.RED)
public class Clerk implements ExcelAble {

    @Cell(value = "姓名",groups = {GroupA.class,GroupB.class})
    private String name;

    @Cell(value = "年龄" ,groups = GroupB.class)
    private String age;

    @Cell(value = "收益" ,groups = GroupA.class)
    private Integer income;

    public Clerk() {
    }

    interface GroupA{}
    interface GroupB{}

    public Clerk(String name, String age, Integer income) {
        this.name = name;
        this.age = age;
        this.income = income;
    }
}
