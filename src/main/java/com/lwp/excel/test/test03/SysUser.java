package com.lwp.excel.test.test03;

import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Style;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class SysUser implements Serializable , ExcelAble {
    private Long id;

    /**
     * 账号
     */
    @Cell(value = "商户账号")
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    @Cell(value = "商户昵称")
    private String nickName;

    /**
     * 电话
     */
    private String phone;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 用户积分
     */
    private BigDecimal score;

    /**
     * 提现费率（备用）
     */
    private BigDecimal withdrawRate;

    /**
     * 收支费率
     */
    private BigDecimal rate;

    /**
     * 收支费率类型（0-收入，1-支出）
     */
    private Integer rateType;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 是否可用：0-否，1-是
     */
    private Integer enabled;

    /**
     * 代理商ID
     */
    private Long agentId;

    /**
     * 创建者ID
     */
    private Long leaderId;

    /**
     * 剩余开通/绑定用户数量
     */
    private Integer residueCount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建日期
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;


}