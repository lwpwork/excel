/*
 * Copyright (c) 2018-2022 Caratacus, (caratacus@qq.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.lwp.excel.test.test02VO;



import com.lwp.excel.annotation.Cell;
import com.lwp.excel.annotation.Sheet;
import com.lwp.excel.annotation.Title;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 船舶月度燃油消耗表
 * </p>
 *
 * @author Caratacus
 */
@Data
@Sheet("月度燃油消耗表")
@Title("租用船舶月度燃油消耗统计表")
public class MonthlyFuelConsumption implements ExcelAble  {

    @Cell("船名")
    private String shipName;

    @Cell("上月库存油 (方)")
    private LastMonthInventory lastMonthInventory;

    @Cell("本月加燃料 (方)")
    private ThisMonthReplenishment thisMonthReplenishment;

    @Cell("本月外供油 (方)")
    private ThisMonthSupply thisMonthSupply;

    @Cell("本月自耗油 (方)")
    private ThisMonthPersonal thisMonthPersonal;

    @Cell("库存油 (方)")
    private Inventory inventory;

    @Cell("主机运转时长 (小时)")
    private HostRunningTime hostRunningTime;

    @Cell("总航程 (海里)")
    private BigDecimal totalVoyage;

    @Cell("靠平台次数")
    private Integer berthingFrequency;

    @Cell("备注")
    private String remark;


}
