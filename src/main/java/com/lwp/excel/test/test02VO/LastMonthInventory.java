package com.lwp.excel.test.test02VO;


import com.lwp.excel.annotation.Cell;
import com.lwp.excel.resolver.ExcelAble;
import lombok.Data;

@Data
public class LastMonthInventory implements ExcelAble {
    @Cell("0#")
    private String oils0;
    @Cell("-10#")
    private String oils10;
    @Cell("合计")
    private String summation;
}
