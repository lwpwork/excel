package com.lwp.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 对应title的值：
 * 一个Sheet对应一个Title。
 * 如果有级联报表，那么就不允许再次解析Title
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Title {

    /**
     * Excel表明
     * @return
     */
    String value() default "数据列表";

    /**
     * 标题行高
     * @return
     */
    short heightInPoints() default 20;


}